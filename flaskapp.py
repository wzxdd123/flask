from flask import Flask, redirect, jsonify, request
from collections import Counter
import psycopg2
from flask_awscognito import AWSCognitoAuthentication
import boto3
from botocore.config import Config

my_config = Config(
    region_name = 'us-west-2',
    signature_version = 'v4',
    retries = {
        'max_attempts': 10,
        'mode': 'standard'
    }
)

app = Flask(__name__)

app.config['AWS_DEFAULT_REGION'] = 'us-west-2'
app.config['AWS_COGNITO_DOMAIN'] = 'https://demo-cog.auth.us-west-2.amazoncognito.com/'
app.config['AWS_COGNITO_USER_POOL_ID'] = 'us-west-2_Rrr5oWE12'
app.config['AWS_COGNITO_USER_POOL_CLIENT_ID'] = 'gqa0f0vt4nvmjmdnf9ubm8kk4'
# app.config['AWS_COGNITO_USER_POOL_CLIENT_SECRET'] = 'v2hfd79scs84p2uhu5pne5cbqs8j4bvrsqj0jmvcs6mjev1244a'
app.config['AWS_COGNITO_USER_POOL_CLIENT_SECRET'] = ''
app.config['AWS_COGNITO_REDIRECT_URL'] = 'https://acloudserve.com/loggedin'



aws_auth = AWSCognitoAuthentication(app)


@app.route('/')
def hello_world():
        home='''
		<!doctype html>
			<html lang="en">
			<head>
					<title>DEMO APP</title>
			</head>

			<body>
			<h1>Welcome to the DEMO APP</h1>
			<form method="get" action="https://acloudserve.com/login">
					<input type="submit" value="Login" />
			</form>
			<br><br><br>
			<p>In this demo, we are showing off authentication as well as MFA, please refer to below diagram for full use of AWS Cognito</p>

			<img src="https://d2908q01vomqb2.cloudfront.net/0a57cb53ba59c46fc4b692527a38a87c78d84028/2017/07/19/CognitoDiagram.png" alt="Welcome">
			</body>
        '''
        return home

@app.route('/loggedin')
def logged_in_page():
	access_token = aws_auth.get_access_token(request.args)
	client = boto3.client('cognito-idp', config=my_config)
	response = client.get_user(
    	AccessToken=access_token
	)
	return connect_to_rds(response['Username'])
	# return jsonify({'access_token': access_token})


@app.route('/loggedin/<username>')
# @aws_auth.authentication_required
def connect_to_rds(username):
	try:
		connection = psycopg2.connect(user = "postgres",password = "password",host = "data-db.c26timzgkpxe.us-east-1.rds.amazonaws.com",port = "5432",database = "orders")
		cursor = connection.cursor()
		cursor.execute("select * from users where user_name='{}'".format(username))
		record = cursor.fetchone()
		print(record)
		return record[1]
	except:
		return 'no record'

@app.route('/login')
def login():
    return redirect(aws_auth.get_sign_in_url())
	# return redirect('https://demo-cog.auth.us-west-2.amazoncognito.com/login?client_id=gqa0f0vt4nvmjmdnf9ubm8kk4&response_type=code&scope=aws.cognito.signin.user.admin+email+openid+phone+profile&redirect_uri=https://google.com')


if __name__ == '__main__':
  app.run()